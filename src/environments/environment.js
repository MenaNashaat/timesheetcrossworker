// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


export default {
  apiUrl() {
    // if (process.env.VUE_APP_API_URL) {
    //   return process.env.VUE_APP_API_URL;
    // } else {
    //   return "YOU_NEED_TO_CHECK_CONFIGURE_YOUR_APP_IN_ENVIRONMENT.JS";
    // }
    return 'https://jsonplaceholder.typicode.com/'
  },
};
