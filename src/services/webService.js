import axios from "axios";
import environment from "../environments/environment";
import { authHeader } from '../helpers/auth-header';

// import Vue from 'vue';
// import VueToast from 'vue-toast-notification';
// Import one of the available themes
// import 'vue-toast-notification/dist/theme-default.css';
// import 'vue-toast-notification/dist/theme-sugar.css';
// import i18n from "../i18n";


// Vue.use(VueToast);

export default {

  // error(message) {
  //   Vue.$toast.open({
  //     message: message,
  //     type: "error",
  //     duration: 15000,
  //     dismissible: true,
  //     queue: false,
  //     position: "top-right",
  //     onClick: this.onClick,
  //     onDismiss: this.onDismiss,
  //   });
  // },

  post(url, body) {
     var resulte = [];
    let config = {

      headers: authHeader(body)
    }

    resulte = axios.post(`${environment.apiUrl() + url}`, body, config)
    resulte.then((response) => {
      if (response.data.responseCode !== "200") {
        //there is backend error
      }
    },
      resulte.catch(error => {

        // this.error(error)
        return error;
      }),

    );
    return resulte
  },
  get(url, body) {
    var resulte = [];
    let config = { 

      headers: authHeader(body)
    };
    //Object.assign(body, BodyPadding());
    resulte = axios.get(`${environment.apiUrl() + url}`, config)
    resulte.then((response) => {
      if (response.data.responseCode !== "200") {
        //there is backend error
      }
    },
      resulte.catch(error => {

        // this.error(error)
        return error;
      }),

    );
    return resulte
  },

};


